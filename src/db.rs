// A Discord bot for the CordFuck Discord server.
// Copyright (C) 2021 Logan Wemyss

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::sync::atomic::{AtomicBool, Ordering};

use anyhow::Result;
use sqlx::{migrate, pool::PoolOptions, query, MySqlPool};

/// If a connection to the database already exists then this must be true.
static DB_EXISTS: AtomicBool = AtomicBool::new(false);

/// Creates a database pool with a given URI and maximum number of connections.
pub async fn new(uri: String, max_connections: u32) -> Result<MySqlPool, sqlx::Error> {
    let exists = DB_EXISTS.compare_exchange(false, true, Ordering::SeqCst, Ordering::SeqCst); // TODO: Prove Relaxed ordering is fine here.
    if let Err(_) = exists {
        panic!("Database pool already exists! This is a bug.");
    }

    let conn = PoolOptions::new()
        .max_connections(max_connections)
        .after_connect(|conn, _| {
            Box::pin(async {
                query!("SET SESSION sql_mode = 'STRICT_ALL_TABLES'") // It's the rust way.
                    .execute(conn)
                    .await?;

                Ok(())
            })
        })
        .connect(&uri)
        .await?;

    migrate!("./migrations")
        .run(&mut conn.acquire().await?)
        .await?;

    Ok(conn)
}
