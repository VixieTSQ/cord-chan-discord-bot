// A Discord bot for the CordFuck Discord server.
// Copyright (C) 2021 Logan Wemyss

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

//! Handle various events called by serenity.

use std::env;

use anyhow::{Context, Result};
use poise::{
    serenity_prelude::{ArgumentConvert, Member, Reaction},
    Event, FrameworkContext,
};
use serenity::model::id::GuildId;

use crate::{models::ReactionRole, randommsg::say_random_message_every_four_hours, *};

pub async fn handle_event(
    ctx: &serenity::Context,
    event: &poise::Event<'_>,
    framework: &FrameworkContext<'_, MySqlPool, anyhow::Error>,
    database: &MySqlPool,
) -> Result<()> {
    match event {
        Event::Ready { .. } => {
            println!("Connected to Discord.");

            tokio::spawn(say_random_message_every_four_hours(ctx.clone()));

            let commands = &framework.options().commands;

            let create_commands = poise::builtins::create_application_commands(commands);
            match env::var("DEVELOPMENT_SERVER_ID") {
                Ok(id) => {
                    let id = id.parse().context("Invalid development server ID")?;
                    let guild = GuildId(id);
                    println!(
                        "Registering slash commands locally in {}",
                        guild
                            .name(&ctx)
                            .ok_or(anyhow::anyhow!("Invalid development server ID"))?
                    );
                    guild
                        .set_application_commands(ctx, |b| {
                            *b = create_commands;
                            b
                        })
                        .await?;
                }
                Err(_) => {
                    println!("Registering slash commands globally");
                    serenity::Command::set_global_application_commands(ctx, |b| {
                        *b = create_commands;
                        b
                    })
                    .await?;
                }
            }

            Ok(())
        }
        Event::ReactionAdd { add_reaction } => {
            let reaction = ReactionRole::get(
                database,
                add_reaction.message_id.0,
                add_reaction.emoji.clone(),
            )
            .await?;

            match reaction {
                Some(reaction_role) => {
                    let mut member = get_member_from_reaction(ctx, add_reaction).await?;
                    member.add_role(ctx, reaction_role.role_id).await?;

                    Ok(())
                }
                None => Ok(()),
            }
        }
        Event::ReactionRemove { removed_reaction } => {
            let reaction = ReactionRole::get(
                database,
                removed_reaction.message_id.0,
                removed_reaction.emoji.clone(),
            )
            .await?;

            match reaction {
                Some(reaction_role) => {
                    let mut member = get_member_from_reaction(ctx, removed_reaction).await?;
                    member.remove_role(ctx, reaction_role.role_id).await?;

                    Ok(())
                }
                None => Ok(()),
            }
        }
        _ => Ok(()),
    }
}

async fn get_member_from_reaction(ctx: &serenity::Context, reaction: &Reaction) -> Result<Member> {
    let partial_member = reaction.member.as_ref().unwrap();
    let member = Member::convert(
        ctx,
        partial_member.guild_id,
        None,
        partial_member
            .user
            .as_ref()
            .unwrap()
            .id
            .0
            .to_string()
            .as_str(),
    )
    .await?;

    Ok(member)
}
