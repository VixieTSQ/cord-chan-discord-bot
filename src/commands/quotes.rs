// A Discord bot for the CordFuck Discord server.
// Copyright (C) 2021 Logan Wemyss

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use anyhow::{anyhow, bail, Result};
use sqlx::query;

use crate::*;

#[poise::command(prefix_command, subcommands("add", "remove"))]
pub async fn quote(ctx: Context<'_>, name: String) -> Result<()> {
    let conn = ctx.data();

    let quote = query!("SELECT Content FROM Quotes WHERE name = ?", name)
        .fetch_optional(&mut conn.acquire().await?)
        .await?;
    match quote.map(|q| q.Content) {
        Some(args) => {
            ctx.say(args).await?;
            Ok(())
        }
        None => Err(anyhow!("Quote does not exist")),
    }?;

    Ok(())
}

#[poise::command(prefix_command, slash_command)]
/// Add a provided entry to the DB from [name] for the key and [msg] for the value.
async fn add(ctx: Context<'_>, name: String, #[rest] msg: String) -> Result<()> {
    query!("INSERT INTO Quotes(name, content) VALUES (?, ?)", name, msg)
        .execute(ctx.data())
        .await?;

    ctx.say("Done!").await?;

    Ok(())
}

#[poise::command(prefix_command, slash_command)]
/// Remove a provided entry from the DB by it's key given from [name].
/// Err if entry does not exist.
async fn remove(ctx: Context<'_>, name: String) -> Result<()> {
    let res = query!("DELETE FROM Quotes WHERE name = ?", name)
        .execute(ctx.data())
        .await?;

    if !(res.rows_affected() >= 1) {
        bail!("Quote does not exist");
    };

    ctx.say("Done!").await?;

    Ok(())
}
