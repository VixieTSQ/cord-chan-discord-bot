// A Discord bot for the CordFuck Discord server.
// Copyright (C) 2021 Logan Wemyss

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use anyhow::Result;
use poise::serenity_prelude::{Message, ReactionType, RoleId};

use crate::{models::ReactionRole, *};

#[poise::command(ephemeral, slash_command, subcommands("add", "remove"))]
pub async fn reactionrole(ctx: Context<'_>) -> Result<()> {
    ctx.say("Use the reactionrole add or reactionrole remove commands")
        .await?;

    Ok(())
}

#[poise::command(ephemeral, slash_command, required_permissions = "ADMINISTRATOR")]
/// Add a reaction role.
async fn add(ctx: Context<'_>, message: Message, emoji: ReactionType, role: RoleId) -> Result<()> {
    ReactionRole {
        message_id: message.id,
        emoji: emoji.clone(),
        role_id: role,
    }
    .add(ctx.data())
    .await?;
    message.react(ctx, emoji).await?;

    ctx.say("Done!").await?;

    Ok(())
}

#[poise::command(ephemeral, slash_command, required_permissions = "ADMINISTRATOR")]
/// Remove a reaction role.
async fn remove(ctx: Context<'_>, message: Message, emoji: ReactionType) -> Result<()> {
    ReactionRole::remove(ctx.data(), message.id, emoji.clone()).await?;
    message.delete_reaction_emoji(ctx, emoji).await?;

    ctx.say("Done!").await?;

    Ok(())
}
