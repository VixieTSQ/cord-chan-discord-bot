// A Discord bot for the CordFuck Discord server.
// Copyright (C) 2021 Logan Wemyss

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use anyhow::Result;
use poise::serenity_prelude as serenity;
use poise::serenity_prelude::ChannelId;
use rand::SeedableRng;
use std::env;

const MESSAGES: [&'static str; 41] = [
    "I will murder you in your sleep <@!715269250645884949>.",
    "I will murder you in your sleep <@!715269250645884949>.",
    "I will murder you in your sleep <@!715269250645884949>.",
    "Yall up fucking your cords?",
    "Who wants to fuck my cord?",
    "Legalize nuclear bombs!",
    "Wake up CordFuck! It's time for Baby Housing!",
    "Wake up CordFuck! It's time to boost this servers activity!",
    "Go on to https://cordfuck.com/ immediately.",
    "You do know we sell CDs and Cassettes right? Like... why haven't you bought them yet???",
    "<@!205904999644004352>, please let me out of the basement. I hate your stupid fucking music thingy. CordFuck is an awful name.",
    "Wanna hear a fun fact? Did you know that burning jet fuel is not hot enough to burn steel?",
    "A chair is a type of seat, typically designed for one person and consisting of one or more legs, a flat or slightly angled seat and a back-rest. They may be made of wood, metal, or synthetic materials, and may be padded or upholstered in various colors and fabrics.\n\nChairs vary in design. An armchair has armrests fixed to the seat; a recliner is upholstered and features a mechanism that lowers the chair's back and raises into place a footrest; a rocking chair has legs fixed to two long curved slats; and a wheelchair has wheels fixed to an axis under the seat.",
    "Hey guys it's me, Cord-Chan! Also known as xxX_BigTitsTrollz69420_Xxx!",
    "<@!908545368600498228><@!908545368600498228><@!908545368600498228><@!908545368600498228><@!908545368600498228><@!908545368600498228>",
    "nyaaaa mrow mrow mrow :3",
    "I have a big carbon footprint?? Im literally only size 7 dude. Plus, I drive everywhere!",
    "Neil DeGrasse? No thanks, I perfer to stand.",
    "Who decided to name me Cord-chan?? That's like if Little Caesers named their anime mascot 'Caesar-chan'. Talk about lazy and unoriginal.",
    "If I had bigger tits, CordFuck would be 10x more profitable.",
    "Wake up Mr. <@!384472492024266752>. Wake up and smell the Lolicore.",
    "Breakcore is mid. I personally perfer to listen to such artists like Taylor Swift and The Weeknd.",
    "ON 4NEM",
    "On god, Vixie is such a bitch.",
    "We the best music.",
    "Did you the CordFuck Discord server contains the 100% real DJ Khalid?",
    "Swearing in the CordFuck Discord server will be punished with a 4 day ban. No exceptions.",
    "Posting Phonk music in the CordFuck Discord server will be punished with a 4 day ban. No exceptions.",
    "They don't want me to tell you this but CordFuck was secretly a big ploy by Vixie to pay for her Eve Online omega clone subscription.",
    "Do you like cats?",
    "You're my little stinky pinky :3",
    ":D",
    "The red streak in my hair represents the river of blood from all of my fallen enemies.",
    "She Fazbears on my Freddy for Five Nights.",
    "Fuck a cord today!",
    "daisyxkneesocks? More like, daisyxkneeCOCKS!! LOL!!",
    "Fun fact! Higurhashi 420 is CordFuck's worst release!",
    "Sugar, spice, and everything nice\nThese were the ingredients chosen\nTo create the perfect little girls\nBut Professor Utonium accidentally\nAdded two extra ingredients to the concoction--\nChemical X\nAnd...\nChemical CordFuck!\nThus, The Powerpuff Girls were born\nAlong with a secret 4th girl..\nUsing their ultra-super powers\nAnd epic amen breaks and chops\nBlossom, Bubbles, Buttercup, and Cord-Chan\nHave dedicated their lives to fighting crime\nAnd the forces of evil\nAs well as making the most insufferable breakcore of all time",
    "Every day I grow colder.",
    "Once a CordFuck, always fuck.",
    "CHIMPCON 1\nCHIMPCON 1 IT'S AN EMERGENCY"
];

pub async fn say_random_message_every_four_hours(ctx: serenity::Context) -> Result<()> {
    let channel = ChannelId(
        env::var("GENERAL_CHANNEL_ID")
            .expect("environment variable GENERAL_CHANNEL_ID to exist")
            .parse()
            .expect("environment variable GENERAL_CHANNEL_ID to be a valid integer"),
    );
    let mut rng = rand::rngs::SmallRng::from_entropy();
    let mut timer = tokio::time::interval(std::time::Duration::from_secs(3600 * 4));

    loop {
        timer.tick().await;
        let index = rand::Rng::gen_range(&mut rng, 0..MESSAGES.len());
        channel.say(&ctx, MESSAGES[index]).await.ok();
    }
}
