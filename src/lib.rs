// A Discord bot for the CordFuck Discord server.
// Copyright (C) 2021 Logan Wemyss

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

//! A Discord bot for the CordFuck Discord server.

use poise::serenity_prelude as serenity;
use sqlx::MySqlPool;

pub mod commands;
pub mod db;
pub mod error;
pub mod events;
pub mod models;
pub mod randommsg;

type Context<'a> = poise::Context<'a, MySqlPool, anyhow::Error>;
