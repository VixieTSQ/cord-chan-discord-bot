// A Discord bot for the CordFuck Discord server.
// Copyright (C) 2021 Logan Wemyss

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use anyhow::Result;
use poise::serenity_prelude::MessageId;
use poise::serenity_prelude::{ReactionType, RoleId};
use sqlx::{query, MySqlExecutor};

pub struct ReactionRole {
    pub message_id: MessageId,
    pub emoji: ReactionType,
    pub role_id: RoleId,
}

impl ReactionRole {
    /// Get a reaction role from a message ID and emoji ID.
    pub async fn get(
        conn: impl MySqlExecutor<'_>,
        message_id: u64,
        emoji: ReactionType,
    ) -> Result<Option<Self>> {
        let raw = query!(
            "SELECT * FROM ReactionRoles WHERE message_id = ? AND emoji = ?",
            message_id,
            format!("{}", emoji)
        )
        .fetch_optional(conn)
        .await?;

        Ok(raw.map(|f| {
            Self {
                message_id: MessageId(f.message_id),
                emoji: ReactionType::try_from(f.emoji.as_str()).unwrap(), // Probably impossible to be err
                role_id: RoleId(f.role_id),
            }
        }))
    }

    /// Add ReactionRole to the ReactionRole table.
    /// Does not overwrite existing entries.
    pub async fn add(self, conn: impl MySqlExecutor<'_>) -> Result<()> {
        println!("{}", self.emoji);
        query!(
            "INSERT IGNORE INTO ReactionRoles(message_id, emoji, role_id) VALUES (?, ?, ?)",
            self.message_id.0,
            format!("{}", self.emoji),
            self.role_id.0
        )
        .execute(conn)
        .await?;

        Ok(())
    }

    /// Removes corresponding entries.
    pub async fn remove(
        conn: impl MySqlExecutor<'_>,
        message_id: MessageId,
        emoji: ReactionType,
    ) -> Result<()> {
        query!(
            "DELETE FROM ReactionRoles WHERE message_id = ? AND emoji = ?",
            message_id.0,
            format!("{}", emoji)
        )
        .execute(conn)
        .await?;

        Ok(())
    }
}
