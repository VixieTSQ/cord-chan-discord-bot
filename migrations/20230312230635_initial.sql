ALTER DATABASE cordchan CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS Quotes (
    name        CHAR(255) PRIMARY KEY,
    content     TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS ReactionRoles (
    message_id   BIGINT UNSIGNED,
    emoji        CHAR(255),
    role_id      BIGINT UNSIGNED NOT NULL,
    PRIMARY KEY (message_id, emoji)
);